import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int first = 0;
        int second = 0;
        int third = 0;
        int fourth = 0;
        Scanner sc = new Scanner(System.in);

        first = sc.nextInt();
        second = sc.nextInt();
        third = sc.nextInt();
        fourth = sc.nextInt();

        System.out.println(max2(max2(first,second), max2(third, fourth)));

    }

    public static int max2(int a, int b) {
        if (a > b)
            return a;
        else
            return b;
    }
}
