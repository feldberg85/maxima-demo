import java.util.Scanner;

public class ATM  {
    public static void main(String[] args) {
        int note5000 = 2;
        int note2000 = 1;
        int note1000 = 1;
        int note500 = 1;
        int note200 = 1;
        int note100 = 0;
        int note50 = 1;
        int note10 = 1;
        int note5 = 1;
        int note2 = 1;
        int note1 = 1;
        int inputNumber = 0;
        Scanner sc = new Scanner(System.in);
        inputNumber = sc.nextInt();

        if (5000 * note5000 + 2000 * note2000 + 1000 * note1000 +
                500 * note500 + 200 * note200 + 100 * note100 + 50 * note50 + 10 * note10 + 5 * note5 + 2 * note2 + 1 * note1 == inputNumber)
            System.out.println(note5000 + note2000 + note1000 + note500 + note200 + note100 + note50 + note10 + note5 + note2 + note1);

    }
}
/*
Пусть у нас есть банкомат, который выдает деньги разными купюрами. Необходимо
считать запрашиваемую сумму и вывести количество купюр, которые нужно выдать пользователю

13 768

5 000 - 2 +
2 000 - 1 +
1 000 - 1 +
500 - 1 +
200 - 1 +
100 - 0
50 - 1
10 - 1
5 - 1
2 - 1
1 - 1

 */
